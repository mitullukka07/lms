<?php

namespace App\Http\Controllers;

use App\DataTables\StudentDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
   

    public function profile()
    {
        $admin = Auth::guard('admin')->user();
        return view('admin.profile',compact('admin'));
    }

    public function updateprofile(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        $admin->name = $request->name;
        $admin->save();
        return response()->json('1');
        
    }

    public function list(StudentDataTable $studentDataTable)
    {
        return  $studentDataTable->render('admin.studentlist') ;     
    }
}
