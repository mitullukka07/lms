<?php

namespace App\Http\Controllers;

use App\DataTables\BookDataTable;
use App\Http\Requests\Book\StoreRequest;
use App\Http\Requests\Book\UpdateRequest;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;

class BookController extends Controller
{
    
    public function index(BookDataTable $bookDataTable)
    {
        return  $bookDataTable->render('admin.book.index') ;     
    }

    public function create()
    {
        $category = Category::select('id','name')->get();
        return view('admin.book.create',compact('category'));
    }

    public function store(StoreRequest $request)
    {
        $cover_image  = uploadFile($request->cover_image, 'app/public');
        $randomNumber = random_int(100000, 999999);
        $book = new Book;
        $book->title = $request->title;
        $book->category_id = $request->category_id;
        $book->author = $request->author;
        $book->isbn_number = $randomNumber;
        $book->no_of_copy = $request->no_of_copy;
        $book->status = $request->status;
        $book->publish_year = $request->publish;
        $book->language = $request->language;
        $book->cover_image = $cover_image;        
        $book->save();
        return response()->json(['book'=>$book]);
    }

    public function show(Book $book)
    {
        
    }

    public function edit($id)
    {        
        $book = Book::find($id);
        return response()->json($book);
    }

    public function update(UpdateRequest $request, Book $book)
    {
        $book = Book::find($request->id);
        $update = [
            'title'          =>  empty($request->title) ? $book->title : $request->title,
            'author'         =>  empty($request->author) ? $book->author : $request->author,
            'no_of_copy'     =>  empty($request->no_of_copy) ? $book->no_of_copy : $request->no_of_copy,
            'status'         =>  empty($request->status) ? $book->status : $request->status,
            'publish_year'   =>  empty($request->publish_year) ? $book->publish_year : $request->publish_year,
            'language'       =>  empty($request->language) ? $book->language : $request->language,
        ];
        Book::where('id', $request->id)->update($update);
        $book->update();
        return response()->json('Update');
    }

    public function destroy(Request $request)
    {
        $book = Book::where('id',$request->id)->delete();
        return redirect()->route('admin.book.index')->with('delete',"Delete Successfully");
    }
}
