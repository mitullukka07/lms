<?php

namespace App\Http\Controllers;

use App\DataTables\CategoryDataTable;
use App\Http\Requests\Category\StoreRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{   
    public function index(CategoryDataTable $categoryDataTable)
    {
        return  $categoryDataTable->render('admin.category.index') ;     
    }

    public function create()
    {        
        return view('admin.category.create');
    }

    public function store(StoreRequest $request)
    {
        $category = new Category;
        $category->name = $request->category;
        $category->save();
        return response()->json(['category'=>$category]);
    }

    public function show(category $category)
    {
        
    }

    public function edit($id)
    {
        $category = Category::find($id);        
        return response()->json($category);
    }

    public function update(Request $request, category $category)
    {
        //
    }

    public function destroy(Request $request)
    {
        $category = Category::where('id',$request->id)->delete();
        return redirect()->route('admin.category.index')->with('delete',"Delete Successfully");
    }
}
