<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = "/user/index";
    public function __construct()
    {
        $this->middleware('guest:student')->except('logout');
    }

    public function showLogin()
    {
        return view('user.login');
    }

    protected function attemptLogin(Request $request)
    {     
        $credentials = $request->only('email', 'password');
        if (Auth::guard('student')->attempt($credentials)) {
            $businessData=Student::where('email',$request->email);
            return response()->json(['sucess'=>$businessData]);            
        }
    }  

    public function logout(Request $request)
    { 
        $this->guard('student')->logout();
        return  redirect()->route('student.login');        
    }

    protected function guard()
    {
        return Auth::guard('student');
    }
}
