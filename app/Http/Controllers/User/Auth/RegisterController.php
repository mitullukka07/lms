<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Register\StoreRequest;
use Illuminate\Http\Request;
use App\Models\Student;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    public function showRegister()
    {
        return view('user.register');
    }
    public function register(StoreRequest $request)
    {
        $student = new Student;
        $student->name = $request->name;
        $student->email = $request->email;
        $student->mobile = $request->mobile;
        $student->password= Hash::make($request->password);
        $student->save();
        return response()->json($student);
    }
}
