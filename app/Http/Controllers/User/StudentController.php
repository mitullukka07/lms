<?php

namespace App\Http\Controllers\User;

use App\DataTables\BookDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function index(BookDataTable $bookDataTable)
    {
        return  $bookDataTable->render('user.book.index') ;     
    }

    
    public function profile()
    {
        $admin = Auth::guard('student')->user();
        return view('user.profile',compact('admin'));
    }

    public function updateprofile(Request $request)
    {
        
        $student = Auth::guard('student')->user();
        $student->name = $request->name;
        if ($request->hasFile('image')) {
            $destination = 'trainee/' . $student->image;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('image');
            $name = $file->getClientOriginalName();
            $file->move('trainee/', $name);
            $student->image = $name;
        }
        $student->image = $name;
        if($student->save())
        {
            return redirect()->back();
        }
    }
}
