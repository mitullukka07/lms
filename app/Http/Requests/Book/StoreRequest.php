<?php

namespace App\Http\Requests\Book;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {
        return [
            'title'=>'required',
            'author'=>'required',
            'no_of_copy'=>'required|numeric',
            'publish'=>'required',
            'language'=>'required',
            'cover_image'=>'required|mimes:jpeg,png,jpg'
        ];
    }
}
