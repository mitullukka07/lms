<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title','category_id','author','isbn_number','no_of_copy','status','publish_year','language','cover_image'
    ];

    public function getCoverImageAttribute($value)
    {
        return $value ? asset('storage/app/public' . '/' . $value) : NULL;
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
}
