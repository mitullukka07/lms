@extends('layouts.user.master')
@section('title','Profile')
@section('content') 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Student profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
       
        <!-- /.col -->
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">              
              <li><a href="#settings" data-toggle="tab">Profile</a></li>
            </ul>
            <div class="tab-content">
              
              <div  id="settings">
                <form class="form-horizontal" id="profileupdate" enctype="multipart/form-data" >
                    @csrf
                  <div class="form-group">
                    <label for="inputName"  class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name="name" value="{{$admin->name}}" id="inputName" placeholder="Name">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputName"  class="col-sm-2 control-label">Mobile</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name="mobile" value="{{$admin->mobile}}" readonly id="inputMobile" placeholder="Name">
                    </div>
                  </div>
                
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" value="{{$admin->email}}" readonly id="inputEmail" placeholder="Email">
                    </div>
                  </div>   
                  
                  <div class="form-group">
                    <label for="inputName"  class="col-sm-2 control-label">Image</label>
                    <div class="col-sm-10">
                      <input type="file" class="form-control"  name="image" value="{{$admin->image}}"  id="image">
                      <img src="{{$admin->image}}" width="50px" height="50px">
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" id="update" class="btn btn-danger">Update</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>   
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@endsection

@push('js')

<script>
    $("#update").click(function(e) {
        
        e.preventDefault();
        var form = $("#profileupdate")[0];
        var data = new FormData(form);
        $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            url: '{{route("student.admin.profile")}}',
            data: data,
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                toastr.success('Profile Update Successfully');
            },           
        });
        
    });
</script>

@endpush