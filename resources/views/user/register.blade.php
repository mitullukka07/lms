<!DOCTYPE html>
<html>

<!-- Mirrored from adminlte.io/themes/AdminLTE/pages/examples/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Jan 2022 19:51:09 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Registration Page</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link rel="shortcut icon" href="{{ asset('asset/img/library_black.png') }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/blue.css')}}">
  <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>SignUp</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new student</p>

    <form id="myform" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="name" placeholder="Enter name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="mobile" placeholder="Mobile">
        <span class="glyphicon glyphicon-mobile form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="row">
        <!-- <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div> -->
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" id="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <a href="{{route('student.login')}}" class="text-center">I already have a membership</a>
  </div>
</div>
<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
<script src="{{asset('asset/js/icheck.min.js')}}"></script>

</body>
</html>
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>
<script>
  $(document).ready(function() {
    $("#submit").click(function(e) {
      e.preventDefault();
      var form = $("#myform")[0];
      var data = new FormData(form);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '{{route("student.register")}}',
        type: 'post',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function(result) {
            toastr.success('Register Successfully');
            window.location.href = '{{route("student.login")}}';
        },
        error: function(result) {
          $.each(result.responseJSON.errors, function(i, error) {
            $('.' + i + 'error').empty();
            var el = $(document).find('[name="' + i + '"]');
            el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
          });
        }
      })
    });
  })

 
</script>