<?php

use App\DataTables\PropertyFeaturesDataTable;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\Auth\LoginController;

Route::group(['namespace' => 'Auth'], function () {
    # Login Routes
    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('admin/logout', 'LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'auth:admin'], function () {    
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    Route::group(['prefix'=>'admin'],function(){
        Route::get('profile',[AdminController::class,'profile'])->name('admin.profile');
        Route::post('profile',[AdminController::class,'updateprofile']);
        Route::get('changepassword',[AdminController::class,'changepassword'])->name('changepassword');
        Route::post('changepassword',[AdminController::class,'updatepassword']);
        Route::get('list',[AdminController::class,'list'])->name('list');
    });

    Route::group(['prefix'=>'category'],function(){
        Route::get('create',[CategoryController::class,'create'])->name('category.create');
        Route::get('index',[CategoryController::class,'index'])->name('category.index');
        Route::post('store',[CategoryController::class,'store'])->name('category.store');
        Route::get('edit/{id}',[CategoryController::class,'edit'])->name('category.edit');
        Route::post('update',[CategoryController::class,'update'])->name('category.update');
        Route::post('destroy',[CategoryController::class,'destroy'])->name('category.destroy');
    });


    Route::group(['prefix'=>'book'],function(){
        Route::get('create',[BookController::class,'create'])->name('book.create');
        Route::get('index',[BookController::class,'index'])->name('book.index');
        Route::post('store',[BookController::class,'store'])->name('book.store');
        Route::get('edit/{id}',[BookController::class,'edit'])->name('book.edit');
        Route::post('update',[BookController::class,'update'])->name('book.update');
        Route::post('destroy',[BookController::class,'destroy'])->name('book.destroy');
    });
    
});


?>
