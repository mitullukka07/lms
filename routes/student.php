<?php

use App\DataTables\PropertyFeaturesDataTable;
use App\Http\Controllers\User\Auth\RegisterController;
use App\Http\Controllers\User\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\StudentController;


Route::group(['namespace' => 'Auth'], function () {
    # Login Routes
    Route::get('register',[RegisterController::class,'showRegister']);
    Route::post('register',[RegisterController::class,'Register'])->name('register');
    Route::get('login',[LoginController::class,'showLogin']);
    Route::post('login',[LoginController::class,'attemptLogin'])->name('login');
    Route::post('logout',[LoginController::class,'logout'])->name('logout');
});

Route::group(['middleware' => 'auth:student'], function () {
    Route::get('/dashboard', function () {
        return view('user.dashboard');
    })->name('dashboard');


    Route::group(['prefix'=>'student'],function(){
        Route::get('profile',[StudentController::class,'profile'])->name('admin.profile');
        Route::post('profile',[StudentController::class,'updateprofile']);
        Route::get('create',[StudentController::class,'create'])->name('student.create');
        Route::get('index',[StudentController::class,'index'])->name('student.index');
    });
});
?>
